## How to patch metaverse-api to turn off out of bound highlighting

In `node_modules/metaverse-api/artifacts/preview.js`, search for `highlight:16711680` and remove it.
